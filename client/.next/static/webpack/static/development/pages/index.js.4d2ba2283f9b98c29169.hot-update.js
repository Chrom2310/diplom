webpackHotUpdate("static/development/pages/index.js",{

/***/ "./api/index.jsx":
/*!***********************!*\
  !*** ./api/index.jsx ***!
  \***********************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_corejs2_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime-corejs2/regenerator */ "./node_modules/@babel/runtime-corejs2/regenerator/index.js");
/* harmony import */ var _babel_runtime_corejs2_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_corejs2_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _babel_runtime_corejs2_helpers_esm_objectSpread__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @babel/runtime-corejs2/helpers/esm/objectSpread */ "./node_modules/@babel/runtime-corejs2/helpers/esm/objectSpread.js");
/* harmony import */ var _babel_runtime_corejs2_helpers_esm_asyncToGenerator__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @babel/runtime-corejs2/helpers/esm/asyncToGenerator */ "./node_modules/@babel/runtime-corejs2/helpers/esm/asyncToGenerator.js");
/* harmony import */ var _babel_runtime_corejs2_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @babel/runtime-corejs2/helpers/esm/classCallCheck */ "./node_modules/@babel/runtime-corejs2/helpers/esm/classCallCheck.js");
/* harmony import */ var _babel_runtime_corejs2_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @babel/runtime-corejs2/helpers/esm/createClass */ "./node_modules/@babel/runtime-corejs2/helpers/esm/createClass.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! axios */ "./node_modules/axios/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var generate_password__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! generate-password */ "./node_modules/generate-password/main.js");
/* harmony import */ var generate_password__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(generate_password__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var next_config__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! next/config */ "./node_modules/next-server/dist/lib/runtime-config.js");
/* harmony import */ var next_config__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(next_config__WEBPACK_IMPORTED_MODULE_7__);









var _getConfig = next_config__WEBPACK_IMPORTED_MODULE_7___default()(),
    serverRuntimeConfig = _getConfig.serverRuntimeConfig,
    publicRuntimeConfig = _getConfig.publicRuntimeConfig;

var BASE_URL = publicRuntimeConfig.BASE_URL;

var Api =
/*#__PURE__*/
function () {
  function Api(_ref) {
    var BASE_URL = _ref.BASE_URL;

    Object(_babel_runtime_corejs2_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_3__["default"])(this, Api);

    this.base_url = BASE_URL;
    this.headers = {
      'Access-Control-Allow-Origin': '*'
    };
    this.list = {
      createUser: {
        method: "post",
        pach: "/api/v1/createUser"
      },
      AuthUser: {
        method: "post",
        pach: "/api/v1/AuthUser/"
      },
      createPotoc: "/api/v1/createPotoc"
    };
  }

  Object(_babel_runtime_corejs2_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_4__["default"])(Api, [{
    key: "createUser",
    value: function () {
      var _createUser = Object(_babel_runtime_corejs2_helpers_esm_asyncToGenerator__WEBPACK_IMPORTED_MODULE_2__["default"])(
      /*#__PURE__*/
      _babel_runtime_corejs2_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee(_ref2) {
        var body, result;
        return _babel_runtime_corejs2_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                body = _ref2.body;
                console.log(body, 'v');
                _context.next = 4;
                return axios__WEBPACK_IMPORTED_MODULE_5___default.a.request({
                  method: 'post',
                  url: this.base_url + '/api/v1/createUser',
                  headers: Object(_babel_runtime_corejs2_helpers_esm_objectSpread__WEBPACK_IMPORTED_MODULE_1__["default"])({}, this.headers, {
                    'Accept': 'application/json',
                    'Content-Type': "application/json"
                  }),
                  data: body
                });

              case 4:
                result = _context.sent;
                return _context.abrupt("return", result.data);

              case 6:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this);
      }));

      function createUser(_x) {
        return _createUser.apply(this, arguments);
      }

      return createUser;
    }()
  }, {
    key: "AuthUser",
    value: function () {
      var _AuthUser = Object(_babel_runtime_corejs2_helpers_esm_asyncToGenerator__WEBPACK_IMPORTED_MODULE_2__["default"])(
      /*#__PURE__*/
      _babel_runtime_corejs2_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee2(_ref3) {
        var login, result;
        return _babel_runtime_corejs2_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                login = _ref3.login;
                console.log(login, 'lo');
                _context2.next = 4;
                return axios__WEBPACK_IMPORTED_MODULE_5___default.a.request({
                  method: "get",
                  url: this.base_url + '/api/v1/AuthUser/' + login,
                  headers: Object(_babel_runtime_corejs2_helpers_esm_objectSpread__WEBPACK_IMPORTED_MODULE_1__["default"])({}, this.headers)
                });

              case 4:
                result = _context2.sent;
                console.log(result, 'result');
                return _context2.abrupt("return", result.data);

              case 7:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2, this);
      }));

      function AuthUser(_x2) {
        return _AuthUser.apply(this, arguments);
      }

      return AuthUser;
    }()
  }, {
    key: "GetAccount",
    value: function () {
      var _GetAccount = Object(_babel_runtime_corejs2_helpers_esm_asyncToGenerator__WEBPACK_IMPORTED_MODULE_2__["default"])(
      /*#__PURE__*/
      _babel_runtime_corejs2_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee3(_ref4) {
        var token, query, params, result;
        return _babel_runtime_corejs2_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                token = _ref4.token;
                query = {
                  "token": token
                };
                params = {
                  query: query
                };
                console.log(this.base_url + '/api/v1/account', 'result');
                _context3.next = 6;
                return axios__WEBPACK_IMPORTED_MODULE_5___default.a.request({
                  method: "get",
                  url: this.base_url + '/api/v1/account',
                  headers: Object(_babel_runtime_corejs2_helpers_esm_objectSpread__WEBPACK_IMPORTED_MODULE_1__["default"])({}, this.headers),
                  params: params
                });

              case 6:
                result = _context3.sent;
                return _context3.abrupt("return", result.data.length > 0 ? result.data[0] : false);

              case 8:
              case "end":
                return _context3.stop();
            }
          }
        }, _callee3, this);
      }));

      function GetAccount(_x3) {
        return _GetAccount.apply(this, arguments);
      }

      return GetAccount;
    }()
  }, {
    key: "postAccount",
    value: function () {
      var _postAccount = Object(_babel_runtime_corejs2_helpers_esm_asyncToGenerator__WEBPACK_IMPORTED_MODULE_2__["default"])(
      /*#__PURE__*/
      _babel_runtime_corejs2_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee4(_ref5) {
        var form, ac;
        return _babel_runtime_corejs2_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee4$(_context4) {
          while (1) {
            switch (_context4.prev = _context4.next) {
              case 0:
                form = _ref5.form;
                form.password = generate_password__WEBPACK_IMPORTED_MODULE_6___default.a.generate({
                  length: 10,
                  numbers: true
                });
                form.login = generate_password__WEBPACK_IMPORTED_MODULE_6___default.a.generate({
                  length: 16,
                  numbers: false,
                  uppercase: true
                });
                _context4.next = 5;
                return axios__WEBPACK_IMPORTED_MODULE_5___default.a.request({
                  method: "post",
                  url: this.base_url + '/api/v1/potoks',
                  headers: Object(_babel_runtime_corejs2_helpers_esm_objectSpread__WEBPACK_IMPORTED_MODULE_1__["default"])({}, this.headers, {
                    'Accept': 'application/json',
                    'Content-Type': "application/json"
                  }),
                  data: body
                });

              case 5:
                ac = _context4.sent;

              case 6:
              case "end":
                return _context4.stop();
            }
          }
        }, _callee4, this);
      }));

      function postAccount(_x4) {
        return _postAccount.apply(this, arguments);
      }

      return postAccount;
    }()
  }, {
    key: "upAccount",
    value: function () {
      var _upAccount = Object(_babel_runtime_corejs2_helpers_esm_asyncToGenerator__WEBPACK_IMPORTED_MODULE_2__["default"])(
      /*#__PURE__*/
      _babel_runtime_corejs2_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee5(_ref6) {
        var id, form, result;
        return _babel_runtime_corejs2_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee5$(_context5) {
          while (1) {
            switch (_context5.prev = _context5.next) {
              case 0:
                id = _ref6.id, form = _ref6.form;
                _context5.next = 3;
                return axios__WEBPACK_IMPORTED_MODULE_5___default.a.request({
                  method: 'post',
                  url: this.base_url + '/api/v1/upUser',
                  headers: Object(_babel_runtime_corejs2_helpers_esm_objectSpread__WEBPACK_IMPORTED_MODULE_1__["default"])({}, this.headers, {
                    'Accept': 'application/json',
                    'Content-Type': "application/json"
                  }),
                  data: {
                    id: id,
                    form: form
                  }
                });

              case 3:
                result = _context5.sent;
                return _context5.abrupt("return", result.data);

              case 5:
              case "end":
                return _context5.stop();
            }
          }
        }, _callee5, this);
      }));

      function upAccount(_x5) {
        return _upAccount.apply(this, arguments);
      }

      return upAccount;
    }()
  }, {
    key: "aut",
    value: function () {
      var _aut = Object(_babel_runtime_corejs2_helpers_esm_asyncToGenerator__WEBPACK_IMPORTED_MODULE_2__["default"])(
      /*#__PURE__*/
      _babel_runtime_corejs2_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee6(_ref7) {
        var body, result;
        return _babel_runtime_corejs2_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee6$(_context6) {
          while (1) {
            switch (_context6.prev = _context6.next) {
              case 0:
                body = _ref7.body;
                console.log(body, 'body');
                _context6.next = 4;
                return axios__WEBPACK_IMPORTED_MODULE_5___default.a.request({
                  method: "get",
                  url: this.base_url + '/api/v1/aut',
                  headers: Object(_babel_runtime_corejs2_helpers_esm_objectSpread__WEBPACK_IMPORTED_MODULE_1__["default"])({}, this.headers),
                  params: body
                });

              case 4:
                result = _context6.sent;
                return _context6.abrupt("return", result.data);

              case 6:
              case "end":
                return _context6.stop();
            }
          }
        }, _callee6, this);
      }));

      function aut(_x6) {
        return _aut.apply(this, arguments);
      }

      return aut;
    }()
  }, {
    key: "getPotoks",
    value: function () {
      var _getPotoks = Object(_babel_runtime_corejs2_helpers_esm_asyncToGenerator__WEBPACK_IMPORTED_MODULE_2__["default"])(
      /*#__PURE__*/
      _babel_runtime_corejs2_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee7(_ref8) {
        var login, params, result;
        return _babel_runtime_corejs2_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee7$(_context7) {
          while (1) {
            switch (_context7.prev = _context7.next) {
              case 0:
                login = _ref8.login;
                params = {
                  query: {
                    parent: login
                  }
                };
                _context7.next = 4;
                return axios__WEBPACK_IMPORTED_MODULE_5___default.a.request({
                  method: "get",
                  url: this.base_url + '/api/v1/potoks',
                  headers: Object(_babel_runtime_corejs2_helpers_esm_objectSpread__WEBPACK_IMPORTED_MODULE_1__["default"])({}, this.headers),
                  params: params
                });

              case 4:
                result = _context7.sent;
                return _context7.abrupt("return", result.data);

              case 6:
              case "end":
                return _context7.stop();
            }
          }
        }, _callee7, this);
      }));

      function getPotoks(_x7) {
        return _getPotoks.apply(this, arguments);
      }

      return getPotoks;
    }()
  }, {
    key: "postPotoks",
    value: function () {
      var _postPotoks = Object(_babel_runtime_corejs2_helpers_esm_asyncToGenerator__WEBPACK_IMPORTED_MODULE_2__["default"])(
      /*#__PURE__*/
      _babel_runtime_corejs2_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee8(_ref9) {
        var body, result;
        return _babel_runtime_corejs2_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee8$(_context8) {
          while (1) {
            switch (_context8.prev = _context8.next) {
              case 0:
                body = _ref9.body;
                _context8.next = 3;
                return axios__WEBPACK_IMPORTED_MODULE_5___default.a.request({
                  method: "post",
                  url: this.base_url + '/api/v1/potoks',
                  headers: Object(_babel_runtime_corejs2_helpers_esm_objectSpread__WEBPACK_IMPORTED_MODULE_1__["default"])({}, this.headers, {
                    'Accept': 'application/json',
                    'Content-Type': "application/json"
                  }),
                  data: body
                });

              case 3:
                result = _context8.sent;
                return _context8.abrupt("return", result.data);

              case 5:
              case "end":
                return _context8.stop();
            }
          }
        }, _callee8, this);
      }));

      function postPotoks(_x8) {
        return _postPotoks.apply(this, arguments);
      }

      return postPotoks;
    }()
  }]);

  return Api;
}();

var api = new Api({
  BASE_URL: BASE_URL
});
/* harmony default export */ __webpack_exports__["default"] = (api);

/***/ })

})
//# sourceMappingURL=index.js.4d2ba2283f9b98c29169.hot-update.js.map