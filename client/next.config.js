const withSass = require('@zeit/next-sass');

const env = process.env;

module.exports = withSass({
    publicRuntimeConfig: { // Will be available on both server and client
        BASE_URL :  env.BASE_URL || 'http://192.168.0.4:3001',
        API_KEY : env.API_KEY || "none"
      }
})