import { Component } from 'react';
import api from '../api';
import BlockForma from '../componets/FormBlock';
import '../static/scss/index.scss';
import Promise from 'bluebird';
import {message} from 'antd';

export default class Registration extends Component{

    onClick=async()=>{
        let login = document.getElementById('login').value;
        let password = document.getElementById('password').value;
        let email = document.getElementById('email').value;
        let role = 0;
        if (login!=""&&password!=""&&email!=""){
            api.createUser({body:{
                login,
                password,
                email,
                role
            }})
        }
    }
    handleOk=async()=>{
        const validateForm = Promise.promisify(this.form.validateFields)
       
        let formData = await validateForm()
        if (formData.remember){
            formData.role = Number(formData.role);
            console.log(formData,'formData');
            let res = await api.createUser({body:formData})
            if (res === 200){
                message.success('Сообщение с потверждением отправлено на '+formData.email)
            }else{
                message.error('Ошибка')
            }
        }else{
            message.error('Для продолжения авторизации необходимо согласится с политикой')
        }
        
        
    }
    setFormRef = (ref) => {
        this.form = ref.props.form;
    }
    render(){
        const forma = [
            {
                type:"login",
                label:"login",
                required:true,
                message:"",
                placeholder:"login"
            },{
                type:"password",
                label:"password",
                required:true,
                message:"",
                placeholder:"pass"
            },{
                type:"email",
                label:"Почта",
                required:true,
                message:"",
                placeholder:"email"
            },{
                type:"role",
                label:'Роль',
                required:true,
                message:"",
                placeholder:"email",
                opt:[{
                    text:"Абитуриент",
                    value:0
                },{
                    text:'Учебное учреждение',
                    value:2
                }]
            }
        ]
        return(
            <div className="page">
                <BlockForma
                handleOk={this.handleOk}
                width={"500px"}
                wrappedComponentRef={this.setFormRef}
                forma={forma}
                />
            </div>
        )
    }
}