import { Component } from 'react';
import Link from 'next/link';
import cookieLib from 'cookie';
import api from '../api';
import '../static/scss/index.scss';
import { message } from 'antd';

export default class IndexPages extends Component{
    
    static async getInitialProps(props){
        var InitialProps = {};
        const cookie = props.req? props.req.headers.cookie:document.cookie;
        let session_id_user = cookie != undefined && cookieLib.parse(cookie).session_id_user? cookieLib.parse(cookie).session_id_user :false;
        const aut = session_id_user ? true : false;
        if (aut){
            InitialProps.account = await api.GetAccount({token:session_id_user});
            if (InitialProps.role == 2){
                InitialProps.valid = InitialProps.nameGroup ? true:false
            } 
            else InitialProps.valid = InitialProps.account? InitialProps.account.predmets?true:false:false;
        }
        InitialProps.aut = aut;
        
        return {InitialProps}
    }

    constructor({InitialProps}){
        super();
        this.state = {
            ...InitialProps,
            login:"",
            password:""
        }

    }
    setCookie=(name, value, options)=>{
        options = options || {};
      
        var expires = options.expires;
      
        if (typeof expires == "number" && expires) {
          var d = new Date();
          d.setTime(d.getTime() + expires * 1000);
          expires = options.expires = d;
        }
        if (expires && expires.toUTCString) {
          options.expires = expires.toUTCString();
        }
      
        value = encodeURIComponent(value);
      
        var updatedCookie = name + "=" + value;
      
        for (var propName in options) {
          updatedCookie += "; " + propName;
          var propValue = options[propName];
          if (propValue !== true) {
            updatedCookie += "=" + propValue;
          }
        }
      
        document.cookie = updatedCookie;
      }

    aut = async()=>{
        const {login,password} = this.state;
        const res = await api.aut({
            body:{
                login,
                password
            }
        })
        console.log(res,'status');
        
        if ( res.status === 200 ){
            this.setCookie('session_id_user',res.user.token,{
                expires:60*60*30*60,
                path:'/'
            })
            message.success("Авторизация прошла успешно")
            setTimeout(() => {
                document.location.replace('/account')    
            }, 3000);
            
        }else{
            message.error('Login или пароль не верны')
        }
    }

    render(){
        const { aut ,valid } = this.state;
        console.log(this.state);
        
        return(
            <div className="page">
                <div className="main">
                    <input onChange={(e)=>this.setState({login:e.target.value})} value={this.state.login} type="text"/>
                    <input onChange={(e)=>this.setState({password:e.target.value})} value={this.state.password} type="password"/>
                    <button onClick={this.aut}>Войти</button>
                </div>
            </div>
        )
    }
}