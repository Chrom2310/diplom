import { Component } from 'react';
import '../../static/scss/index.scss';
import cookieLib from 'cookie';
import api from '../../api';
import SteaperRole0 from '../../componets/SteaperRole0';
import SteaperRole2 from '../../componets/SteaperRole2';
import AccountRole2 from '../../componets/accountRole2';
import AccountRole0 from '../../componets/accountRole0';

export default class AccountPage extends Component{
    
    static async getInitialProps(props){
        var InitialProps = {};
        const cookie = props.req? props.req.headers.cookie:document.cookie;
        let session_id_user = cookie != undefined && cookieLib.parse(cookie).session_id_user? cookieLib.parse(cookie).session_id_user :false;
        const aut = session_id_user ? true : false;
        if (aut){
            InitialProps.account = await api.GetAccount({token:session_id_user});
            if (InitialProps.account.role == 2){
                InitialProps.valid = InitialProps.account.nameGroup ? true:false
            } 
            else InitialProps.valid = InitialProps.account? InitialProps.account.predmets?true:false:false;
        }
        InitialProps.session_id_user = session_id_user;
        
        return {InitialProps}
    }

    constructor({InitialProps}){
        super();
        this.state = {
            ...InitialProps
        }
    }

    sucs = ()=>{
        this.setState({valid:true});
    }

    accountUp = (account)=>{
        this.setState({account})
    }

    render(){
        console.log(this.state);
        const {valid , account , session_id_user } = this.state;
        let ster;
        if (account.role == 2){
            ster = <SteaperRole2 sucs={this.props.sucs} account={account}/>;
        }else{
            ster = <SteaperRole0 sucs={this.props.sucs} account={account}/>;
        }
        let ac ;
        if (account.role == 2){
            ac = <AccountRole2 accountUp={this.accountUp} session_id_user={session_id_user} account={account}/>
        }else ac = <AccountRole0 accountUp={this.accountUp} session_id_user={session_id_user} account={account}/>
        return(
            <div className="page">
            {!valid &&
                <div className="margin">
                    {ster}
                </div>
            }

            {
                valid &&
                <div className="margin">
                    {ac}
                </div>
            }
            </div>
        )
    }
}