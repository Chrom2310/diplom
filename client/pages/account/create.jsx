import { Component } from 'react';
import '../../static/scss/index.scss';
import cookieLib from 'cookie';
import api from '../../api';
import SteperCreate from '../../componets/SteperCreate';

export default class AccountPage extends Component{
    
    static async getInitialProps(props){
        var InitialProps = {};
        const cookie = props.req? props.req.headers.cookie:document.cookie;
        let session_id_user = cookie != undefined && cookieLib.parse(cookie).session_id_user? cookieLib.parse(cookie).session_id_user :false;
        const aut = session_id_user ? true : false;
        if (aut){
            InitialProps.account = await api.GetAccount({token:session_id_user});
            if (InitialProps.account.role == 2){
                InitialProps.valid = InitialProps.account.nameGroup ? true:false
                InitialProps.potoks = await api.getPotoks({login:InitialProps.account.login});
            } 
            else InitialProps.valid = InitialProps.account? InitialProps.account.predmets?true:false:false;
        }
        InitialProps.session_id_user = session_id_user;
        
        return {InitialProps}
    }

    constructor({InitialProps}){
        super();
        this.state = {
            ...InitialProps
        }
    }

    sucs = ()=>{
        this.setState({valid:true});
    }

    accountUp = (account)=>{
        this.setState({account})
    }

    render(){
        console.log(this.state);
        const {valid , account , session_id_user , potoks } = this.state;
        return(
            <div className="page">
                <SteperCreate potoks={potoks} account={account}/>
            </div>
        )
    }
}