import { Component } from 'react';
import api from '../api';
import '../static/scss/index.scss';
import { Alert } from 'antd';

export default class IndexPages extends Component{

    static async getInitialProps(props){
        const query = props.query;
        let cookie = await api.AuthUser({login:query.login});
        // console.log(cookie,'cookie');
        // let cookie = query.login
        return {cookie};
    }

    constructor({cookie}){
        super();
        this.state ={
            cookie:cookie,
            save:false
        }
    }

    getCookie=(name)=> {
        var matches = document.cookie.match(new RegExp(
          "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
        ));
        return matches ? decodeURIComponent(matches[1]) : undefined;
      }

    setCookie=(name, value, options)=>{
        options = options || {};
      
        var expires = options.expires;
      
        if (typeof expires == "number" && expires) {
          var d = new Date();
          d.setTime(d.getTime() + expires * 1000);
          expires = options.expires = d;
        }
        if (expires && expires.toUTCString) {
          options.expires = expires.toUTCString();
        }
      
        value = encodeURIComponent(value);
      
        var updatedCookie = name + "=" + value;
      
        for (var propName in options) {
          updatedCookie += "; " + propName;
          var propValue = options[propName];
          if (propValue !== true) {
            updatedCookie += "=" + propValue;
          }
        }
      
        document.cookie = updatedCookie;
      }

    async componentDidMount(){
        this.setCookie('session_id_user',this.state.cookie.token,{
            expires:3600*24*30
        })        
        this.setState({save:true})
    }

    render(){
        return(
            <div className="page">
                {this.state.save && 
                    <div className="myAler" style={{width:'320px',display:'block',margin:'auto'}}>
                        <Alert
                        message="Подтверждение"
                        description="Подтверждение почты прошло успешно"
                        type="success"
                        >

                        </Alert>
                    </div>
                }      
            </div>
        )
    }
}