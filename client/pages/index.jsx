import { Component } from 'react';
import Link from 'next/link';
import cookieLib from 'cookie';
import api from '../api';
import '../static/scss/index.scss';

export default class IndexPages extends Component{
    
    static async getInitialProps(props){
        var InitialProps = {};
        const cookie = props.req? props.req.headers.cookie:document.cookie;
        let session_id_user = cookie != undefined && cookieLib.parse(cookie).session_id_user? cookieLib.parse(cookie).session_id_user :false;
        const aut = session_id_user ? true : false;
        if (aut){
            InitialProps.account = await api.GetAccount({token:session_id_user});
            if (InitialProps.account.role == 2){
                InitialProps.valid = InitialProps.account.nameGroup ? true:false
            } 
            else InitialProps.valid = InitialProps.account? InitialProps.account.predmets?true:false:false;
        }
        InitialProps.aut = aut;
        
        return {InitialProps}
    }

    constructor({InitialProps}){
        super();
        this.state = {
            ...InitialProps
        }

    }
    setCookie=(name, value, options)=>{
        options = options || {};
      
        var expires = options.expires;
      
        if (typeof expires == "number" && expires) {
          var d = new Date();
          d.setTime(d.getTime() + expires * 1000);
          expires = options.expires = d;
        }
        if (expires && expires.toUTCString) {
          options.expires = expires.toUTCString();
        }
      
        value = encodeURIComponent(value);
      
        var updatedCookie = name + "=" + value;
      
        for (var propName in options) {
          updatedCookie += "; " + propName;
          var propValue = options[propName];
          if (propValue !== true) {
            updatedCookie += "=" + propValue;
          }
        }
      
        document.cookie = updatedCookie;
      }

    deleteCookie=(name)=>{
        this.setCookie(name, "", {
          expires: -1
        })
      }

    quit=()=>{
        this.deleteCookie('session_id_user')
        this.setState({aut:false,valid:false})
    }
    
    render(){
        const { aut ,valid } = this.state;
        console.log(this.state);
        
        return(
            <div className="page">
                {
                    aut ?(
                        <div className="main">
                        {
                            !valid ? (
                                <h1>Вы не полностью прошли регистрацию , зайдите в Аккаунт чтобы полностью ёё пройти</h1>
                            ):(
                                <h1>Вы вошли как {this.state.account.login}</h1>
                            )

                        }
                            <Link href="/account">
                                <a >Акаунт</a>
                            </Link>
                            {
                                this.state.account.role == 0 &&
                                <Link href="/podaca">
                                    <a >Подать документы</a>
                                </Link>
                            }
                            {
                                this.state.account.role == 2 &&
                                <Link href="/account/create">
                                    <a>Добавить студента</a>
                                </Link>
                            }
                            <a onClick={this.quit}>Выйти</a>
                        </div>
                    ):(
                        <div className="main">
                            <Link href="/entry">
                                <a>
                                    Войти
                                </a>
                            </Link>
                            <Link href="/registration">
                                <a>
                                    Регистрация
                                </a>
                            </Link>
                        </div>
                    )
                }
            </div>
        )
    }
}