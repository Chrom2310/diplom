import axios from 'axios';
import generator from'generate-password';

import getConfig from 'next/config'


const {serverRuntimeConfig, publicRuntimeConfig} = getConfig()
const { BASE_URL } = publicRuntimeConfig

class Api {
    constructor({BASE_URL}){
        this.base_url = BASE_URL;
        this.headers = {
            'Access-Control-Allow-Origin':'*'
        }
        this.list = {
            createUser:{
                method:"post",
                pach:"/api/v1/createUser"
            },
            AuthUser:{
                method:"post",
                pach:"/api/v1/AuthUser/"
            },
            createPotoc:"/api/v1/createPotoc"
        }
    }

    async createUser({body}){
        console.log(body,'v');
        
        const result = await axios.request({
            method:'post',
            url: this.base_url+'/api/v1/createUser',
            headers:{
                ...this.headers,
                'Accept': 'application/json',
                'Content-Type':"application/json"
            },
            data:body
        })
        return result.data;
    }

    async AuthUser({login}){
        console.log(login,'lo');
        
        const result = await axios.request({
            method:"get",
            url:this.base_url+'/api/v1/AuthUser/'+login,
            headers:{
                ...this.headers
            }
        })
        console.log(result,'result');
        
        return result.data;
    }

    async GetAccount({token}){
        const query = {
            "token":token
        }
        const params = {
            query
        }
        console.log(this.base_url+'/api/v1/account','result');
        
        const result = await axios.request({
            method:"get",
            url:this.base_url+'/api/v1/account',
            headers:{
                ...this.headers
            },
            params
        })
        
        return result.data.length > 0 ? result.data[0] : false;
    }

    async postAccount({form}){
        form.password = generator.generate({
            length:10,
            numbers:true
        })
        form.login = generator.generate({
            length:16,
            numbers:false,
            uppercase:true
        })
        const ac = await axios.request({
            method:"post",
            url: this.base_url+'/api/v1/account',
            headers:{
                ...this.headers,
                'Accept': 'application/json',
                'Content-Type':"application/json"
            },
            data:form
        })
        if (ac.data){
            const res = await this.AuthUser({login:form.login})
            return ac.data;
        }else return "err"
    }
    
    async upAccount({id,form}){
        const result = await axios.request({
            method:'post',
            url: this.base_url+'/api/v1/upUser',
            headers:{
                ...this.headers,
                'Accept': 'application/json',
                'Content-Type':"application/json"
            },
            data:{
                id,
                form
            }
        })
        return result.data
    }

    async aut({body}){
        console.log(body,'body');
        
        const result = await axios.request({
            method:"get",
            url: this.base_url+'/api/v1/aut',
            headers:{
                ...this.headers
            },
            params:body
        })
        return result.data;
    }
    async getPotoks({login}){

        const params = {
            query:{
                parent:login
            }
        }

        const result = await axios.request({
            method:"get",
            url: this.base_url+'/api/v1/potoks',
            headers:{
                ...this.headers
            },
            params
        })

        return result.data
    }


    async postPotoks({body}){
        const result = await axios.request({
            method:"post",
            url: this.base_url+'/api/v1/potoks',
            headers:{
                ...this.headers,
                'Accept': 'application/json',
                'Content-Type':"application/json"
            },
            data:body
        })
        return result.data;
    }

    async addPotoksStyd({form}){
        const result = await axios.request({
            method:"post",
            url: this.base_url+'/api/v1/addStydent',
            headers:{
                ...this.headers,
                'Accept': 'application/json',
                'Content-Type':"application/json"
            },
            data:form
        })
        return result.data;
    }

}

const api = new Api({BASE_URL});

export default api;