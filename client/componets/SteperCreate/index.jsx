import { Component } from 'react';
import './SteperCteate.scss';
import { Steps , message} from 'antd';
const { Step } = Steps;
import Step1 from './Step1';
import Step2 from './Step2';
import Step3 from './Step3';
import Step4 from './Step4';
import api from '../../api';

export default class SteperCreate extends Component{

    constructor(){
        super();
        this.state = {
            current:0,
            form:{},
            ac:null
        }
    }

    next=()=> {
        const current = this.state.current + 1;
        this.setState({ current });
      }
    
    prev=()=> {
        const current = this.state.current - 1;
        this.setState({ current });
    }

    clickStep1 = (state) =>{
      const form = Object.assign(state,this.state.form);
      this.setState({form});
      this.next();
    }

    suc= async ()=>{
      const { form  } = this.state;
      var postAccount = await api.postAccount({form});
      if (postAccount){
        this.setState({ac:postAccount})
        message.success("пользователь успешно создан")
      }
    }
    
    clickStep2 = (state) =>{
      console.log(typeof state,'state.predmets');
      console.log(state,'state');
      var {form} = this.state;
      console.log(form,'from');
      form['predmets'] = state;
      this.setState({form});
      this.suc();
      this.next()
    }

    clickStep3 = () =>{
      this.next()
    }
    
    clickStep4 = async(potok) =>{
      const form = {
        potok:potok,
        parent:this.props.account.login,
        user:this.state.ac.login
      }
      let res = await api.addPotoksStyd({form});
      if (res){
        message.success('Пользователь добавлен')
      }
    }

    render(){
        const { current } = this.state
        const steps = [
            {
                title: 'Личные данные',
                content: <Step1 click={this.clickStep1}/>,
              },
              {
                title: 'Оценки',
                content: <Step2 click={this.clickStep2} prev={this.prev}/>,
              },
              {
                title: 'Аттестат',
                content: <Step3 click={this.clickStep3} user={this.state.ac?this.state.ac._id:"test"} type={'doc'} />,
              },
              {
                title :"Группа",
                content: <Step4 click={this.clickStep4} potoks={this.props.potoks} />
              }
        ]
        console.log(this.state,'state');
        return(
            <div className="SteperCreate">
              <Steps current={current}>
                {steps.map(item => (
                <Step key={item.title} title={item.title} />
             ))}
             </Steps>
            <div className="steps-content">{steps[current].content}</div>
            </div>
        )
    }
}
