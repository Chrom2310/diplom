import { Component } from 'react';

export default class Step4 extends Component{
    render(){
        const {potoks} = this.props;
        const mapPotoks = potoks.map((val,index)=>{
            return(
                <button onClick={()=>this.props.click(val.name)}>{val.name}</button>
            )
        })
        return(
            <div className="step step4">
                {mapPotoks}
            </div>
        )
    }
}