import { Component } from 'react';

export default class Step2 extends Component{

    constructor(){
        super();
        this.state ={
            predmets:[],
            pole:"",
            number:0
        }
    }
    cler=(pole)=>{
        const {predmets} = this.state;
        const newMas = predmets.filter(el=>el.name != pole);
        this.setState({predmets:newMas});
    }
    add=()=>{
        const {pole,number,predmets}= this.state;
        if (number > 0 && pole != ""){
            predmets.push({
                name:pole,
                number:number
            })
            this.setState({predmets,number:0,pole:""})
        }

    }

    render(){
        const {predmets}= this.state;
        const mapP = predmets.map((val,index)=>{
            return(
                <div key={index}>
                    <span>Предмет: {val.name}</span>
                    <span>Оценка: {val.number}</span>
                    <button onClick={()=>this.cler(val.name)}></button>
                </div>
            )
        })
        const list = ['Русский язык','Литература','Алгебра','География','Физика','Химия',
        'Иностранный язык','Информатика и ИКТ','Экономика','Физическая культура','Всеобщая история'
        ,'История России','Обществознание','БЖД'];
        const opMap = list.map((val,index)=>{
            return(
                <option key={index} value={val}>{val}</option>
            )
        })
        return(
            <div className="step2">
                <div className="predmets">
                    {mapP}
                </div>
                <div className="addPole">
                    <label>
                        <span>Названия Предмета</span>
                        <select onChange={(e)=>this.setState({pole:e.target.value})} value={this.state.pole}>
                            {opMap}
                        </select>
                        {/* <input type="text" onChange={(e)=>this.setState({pole:e.target.value})} value={this.state.pole}/> */}
                    </label>
                    <label>
                        <span>Оценка</span>
                        <input type="number"  onChange={(e)=>this.setState({number:e.target.value})} value={this.state.number}/>
                    </label>
                    <button onClick={this.add} className="add">
                        Добавить
                    </button>
                </div>    
                <button className="next" onClick={()=>this.props.click(this.state.predmets)}>далее</button>
                <button className="prev" onClick={this.props.prev}>Назад</button>
            </div>
        )
    }
}