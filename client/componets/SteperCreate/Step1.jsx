import { Component } from 'react';

export default class Step1 extends Component{

    constructor(){
        super();
        this.state = {
            form:{}
        }
    }
    onChange=(pole,val)=>{
        var { form } = this.state;
        form[pole] = val;
        this.setState({form});
    }

    render(){

        const form = [
            {
                text:"Фамилия",
                pole:"surname",
                type:"text",
                placeholder:""
            },{
                text:"Имя",
                pole:"name",
                type:"text",
                placeholder:""
            },{
                text:"Отчество",
                pole:"patronymic",
                type:"text",
                placeholder:""
            },{
                text:"Серия паспорта",
                pole:"series",
                type:"text",
                placeholder:""
            },{
                text:"Номер Паспорта",
                pole:"number",
                type:"text",
                placeholder:""
            },{
                text:"Номер Аттестата",
                pole:"numAt",
                type:"text",
                placeholder:""
            },{
                text:"Почта",
                pole:"email",
                type:"text",
                placeholder:""
            }
        ]

        const mapForm = form.map((val,index)=>{
            return(
                <label key={index}>
                    <span>{val.text}</span>
                    <input placeholder={val.placeholder} type="text" onChange={(e)=>this.onChange(val.pole,e.target.value)} value={this.state[val.pole]}/>
                </label>
            )
        })
        
        return(
            <div className="step step1">
                {mapForm}
                <button onClick={()=> this.props.click(this.state.form)} className="next">Делее</button>
            </div>
        )
    }
}