import { Component } from 'react';
import { Collapse } from 'antd';
const Panel = Collapse.Panel;

export default class ListPotoks extends Component{
    render(){
        const {potoks} = this.props;
        console.log(potoks,'potoks');
        const mapPanel = potoks.map((val,index)=>{
            return(
                <Panel key={index} header={val.name}>
                {
                    Object.keys(val).map((v,i)=>{
                        return(
                        <span key={i} style={{display:'block'}}>
                            {v}:  {val[v]}
                        </span>
                        )
                        
                    })
                }
                </Panel>
            )
            
        }) 
        return(
            <div className="ListPotoks" style={{marginBottom:"20px;"}}>
            {
                potoks.length > 0 ? (
                    <Collapse>
                        {
                            mapPanel
                        }
                    </Collapse>   
                ):(
                    <h1>
                        Групп нет
                    </h1>
                )
            }
            </div>
        )
    }
}