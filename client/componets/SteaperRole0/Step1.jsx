import { Component } from 'react';

export default class Step1 extends Component{
    
    constructor(){
        super();
        this.state={
            name:"",
            surname:"",
            patronymic:"",
            series:"",
            number:""
        }
    }

    onChange=({val,pole})=>{
        var state = Object.assign({},this.state);
        state[pole] = val;
        this.setState(state);
    }

    render(){
        const maslabel = [
            {
                text:"Фамилия",
                pole:"surname"
            },
            {
                text:"Имя",
                pole:"name"
            },
            {
                text:"Отчество",
                pole:"patronymic"
            },{
                text:"Серия паспорта",
                pole:"series"
            },{
                text:"Номер Паспорта",
                pole:"number"
            },{
                text:"Номер Аттестата",
                pole:"numAt"
            }
        ]
        // console.log(this.state,'state');
        
        const Label = maslabel.map((val,index)=>{
            return(
                <label key={index}>
                    <span>{val.text}</span>
                    <input type="text" onChange={(e)=>this.onChange({val:e.target.value,pole:val.pole})} value={this.state[val.pole]}/>
                </label>
            )
        })

        return(
            <div className="step1">
                {Label}
                <button onClick={()=>this.props.click(this.state)} className="next">Далее</button>
            </div>
        )
    }
}