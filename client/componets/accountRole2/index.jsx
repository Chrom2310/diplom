import { Component } from 'react';
import './accountRole2.scss';
import api from '../../api';
import Link from 'next/link';

export default class AccountRole2 extends Component{
    
    constructor(props){
        super(props);
        this.state = {
            descriptionUp:false,
            description:props.account.description
        }
    }

    upd=async()=>{
        const res = await api.upAccount({id:this.props.account._id,
        form:{
            description:this.state.description
        }}) 
        if (res == 200){
            const account = await api.GetAccount({token:this.props.session_id_user});
            this.props.accountUp(account)
            this.setState({descriptionUp:false})
        }
    }
    
    render(){
        const {descriptionUp} = this.state
        const {account} = this.props;
        const {email,nameGroup,description} = account;
        return(
            <div className="AccountRole2">
                <div>
                    <span>e-mail:</span>
                    {email}
                </div>
                <div>
                    <span>Названия вуза:</span>
                    {nameGroup}
                </div>
                {
                    descriptionUp ? (
                        <div>
                            <span>Описание:</span><button onClick={this.upd}>Сохранить</button>
                            <textarea value={this.state.description} onChange={(e)=>this.setState({description:e.target.value})}></textarea>
                        </div>
                    ):(
                        <div>
                            <span>Описание:</span> <button onClick={()=>this.setState({descriptionUp:true})}>Изменить</button>
                            <div className="textPole" dangerouslySetInnerHTML={{__html:description}}></div>
                        </div>
                    )
                }
                <div>
                    <Link href="/potok">
                        Создать группу
                    </Link>
                </div>
            </div>
        )
    }
}