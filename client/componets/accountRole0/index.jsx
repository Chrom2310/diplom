import { Component } from 'react';
import './accountRole2.scss';
import api from '../../api';
import Link from 'next/link';

export default class AccountRole2 extends Component{
    
    constructor(props){
        super(props);
        this.state = {
            descriptionUp:false,
            description:props.account.description
        }
    }

    upd=async()=>{
        const res = await api.upAccount({id:this.props.account._id,
        form:{
            description:this.state.description
        }}) 
        if (res == 200){
            const account = await api.GetAccount({token:this.props.session_id_user});
            this.props.accountUp(account)
            this.setState({descriptionUp:false})
        }
    }
    
    render(){
        const {descriptionUp} = this.state
        const {account} = this.props;
        const { email,name,surname,patnantmic,login} = account;
        return(
            <div className="AccountRole2">
                <div>
                    <span>Фамилия:</span>
                    {surname}
                </div>
                <div>
                    <span>Имя:</span>
                    {name}
                </div>
                <div>
                    <span>Отчество:</span>
                    {patnantmic}
                </div>
                <div>
                    <span>e-mail:</span>
                    {email}
                </div>
                <div>
                    <span>Логин:</span>
                    {login}
                </div>
            </div>
        )
    }
}