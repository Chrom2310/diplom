import { Component } from 'react';

export default class Step1 extends Component{
    
    constructor(){
        super();
        this.state={
            nameGroup:"",
            description:""
        }
    }

    onChange=({val,pole})=>{
        var state = Object.assign({},this.state);
        state[pole] = val;
        this.setState(state);
    }

    render(){
        const maslabel = [
            {
                text:"Название учебного учреждения",
                pole:"nameGroup"
            },
            {
                text:"Описание",
                pole:"description"
            }
        ]
        // console.log(this.state,'state');
        
        const Label = maslabel.map((val,index)=>{
            return(
                <label key={index}>
                    <span>{val.text}</span>
                    {
                        val.pole != "description"&&
                        <input type="text" onChange={(e)=>this.onChange({val:e.target.value,pole:val.pole})} value={this.state[val.pole]}/>
                    }
                    {
                        val.pole == "description"&&
                        <textarea type="text" onChange={(e)=>this.onChange({val:e.target.value,pole:val.pole})} value={this.state[val.pole]}/>
                    }
                </label>
            )
        })

        return(
            <div className="step1">
                {Label}
                <button onClick={()=>this.props.click(this.state)} className="next">Далее</button>
            </div>
        )
    }
}