import { Component } from 'react';
import { Upload, message, Button, Icon } from 'antd';
import getConfig from 'next/config'
const {serverRuntimeConfig, publicRuntimeConfig} = getConfig()
const { BASE_URL, API_KEY } = publicRuntimeConfig

export default class Step3 extends Component{
    constructor(){
        super();
        this.state={
            file:null
        }
    }
    onResumeSend=(file)=>{
        this.setState({file})
    }
    render(){
        // ,
        // data:{
        //     user:this.props.user,
        //     type:this.props.type
        // },
       const onResumeSend = this.onResumeSend;
        const uploadProps = {
            name: 'file',
            action: BASE_URL+'/api/v1/upload',
            headers: {
              'X-Auth-Key': API_KEY,
              'user':this.props.user,
              'type':this.props.type
            },
            onChange(info) {
              // console.log(info)
              if (info.file.status !== 'uploading') {
                // console.log(info.file, info.fileList);
              }
              if (info.file.status === 'done') {
                message.success(`${info.file.name} file uploaded successfully`);
                if (onResumeSend) onResumeSend(info.file.response)          
              } else if (info.file.status === 'error') {
                message.error(`${info.file.name} file upload failed.`);
              }
            },
        }
        console.log(this.state,'state');
        
        return(
            <div className="step3">
                {
                    this.state.file &&
                    <img style={{width:'100px',height:"auto"}} src={this.state.file.url}/>
                }
                <Upload  {...uploadProps} >
                <Button className="btn_modal">
                    <Icon type="file" />Выберите файл
                </Button>
                </Upload>
                <button onClick={this.props.click} className="next">Закончить</button>
            </div>
        )
    }
}