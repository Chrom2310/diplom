import { Component } from 'react';
import './SteaperRole0.scss';
import { Steps, Button, message } from 'antd';
const { Step } = Steps;
import Step1 from './Step1';
import Step2 from './Step2';
import Step3 from './Step3';
import api from '../../api';

export default class SteaperRole2 extends Component{

    constructor(props) {
        super(props);
        this.state = {
          current: 0,
          form:{}
        };
      }

      next() {
        const current = this.state.current + 1;
        this.setState({ current });
      }
    
      prev() {
        const current = this.state.current - 1;
        this.setState({ current });
      }

      clickStep1 = (state) =>{
        const form = Object.assign(state,this.state.form);
        this.setState({form});
        this.next();
      }
      
      clickStep2 = (state) =>{
        console.log(typeof state,'state.predmets');
        console.log(state,'state');
        var {form} = this.state;
        console.log(form,'from');
        form['predmets'] = state;
        this.setState({form});
        this.next()
      }

       suc = async() =>{
        const {form}  = this.state ;
        let res = await api.upAccount({
          id:this.props.account._id,
          form
        })
        if (res == 200){
          message.success('Успех');
          this.props.sucs();
        }else{
          message.error(res)
        }
      }

    render(){
      console.log(this.props,'props');
      
        const steps = [
            {
              title: 'Учебное учреждение',
              content: <Step1 click={this.clickStep1}/>,
            },
            {
              title: 'Документы',
              content: <Step3 click={this.suc} user={this.props.account._id} type={'docx'}/>,
            },
          ];
          
          const { current } = this.state;
        return(
            <div className="SteaperRole0">
               <Steps current={current}>
                    {steps.map(item => (
                        <Step key={item.title} title={item.title} />
                    ))}
                    </Steps>
                    <div className="steps-content">{steps[current].content}</div>
            </div>
        )
    }
}