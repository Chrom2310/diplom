import {Component} from 'react';
import { Form , Input , Button , Checkbox } from 'antd';
import Role from './Role';
import './FormBlock.scss';

export default Form.create()( class extends Component {
    render(){
        const { forma , width } = this.props;
        const {onResumeSend} = this.props
        const { getFieldDecorator} = this.props.form;
        const mapFromItem = forma.map((val,index)=>{
            return(
                <Form.Item 
                label={val.label}
                key={index}>
                {getFieldDecorator(val.type, {
                  rules: [{ required: val.required, message:val.message }],
                })(
                    <div>
                        {val.type == "role"?(
                            <Role opt={val.opt}/>
                        ):(
                            <Input />
                        )}  
                    </div>
                )}
              </Form.Item>
            )
        })
        return(
            <div className="FormaBlock" style={{width:width}}>
                <Form onSubmit={e=>e.preventDefault()} ref={(c)=> this.subForm = c} >
                    {mapFromItem}
                    <Form.Item>
                        {getFieldDecorator('remember', {
                            valuePropName: 'checked',
                            initialValue: true,
                        })(<Checkbox>Согласен с политекой</Checkbox>)}
                    </Form.Item>
                </Form>
                <button className="send" onClick={this.props.handleOk}>
                    Регистрация
                </button>
            </div>
        )
    }
})