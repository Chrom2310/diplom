import {Component} from 'react';
import { Select } from 'antd';
const { Option } = Select;

export default class Role extends Component{
    render(){
        const {opt} = this.props;
        const mapOption = opt.map((val,index)=>{
            return(
                <option key={index} value={val.value}>
                    {val.text}
                </option>
            )
        })
        return(
            <select>
                {mapOption}
            </select>
        )
    }
}