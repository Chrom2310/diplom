import { Component } from 'react';
import './CreatePotoks.scss';
import api from '../../api';

export default class CreatePotoks extends Component{

    constructor(){
        super();
        this.state = {
            create:false,
            form:{}
        }
    }

    onChange=({pole,val})=>{
        const {form} = this.state;
        form[pole] = val;
        if (pole == "Open"){
            form[pole] = true      
        }
        this.setState({form}); 
    }

    create=async()=>{
        const res = await api.postPotoks({body:{...this.state.form,parent:this.props.account.login}});
        console.log(res,'res');
        
    }

    render(){
        const { create } = this.state;
        const form = [
            {
                span:"Названия",
                pole:"name",
                type:"text"
            },{
                span:"Количество бесплатных мест",
                pole:"NumOfFreePlaces",
                type:"number"
            },{
                span:"Количество платных мест",
                pole:"NumOfPaidPlaces",
                type:"number"
            },{
                span:"Открытость",
                pole:"Open",
                type:"checkbox"
            }
        ]
        const mapForm = form.map((val,index)=>{
            return(
                <label>
                    <span>{val.span}</span>
                    <input type={val.type} onChange={(e)=>this.onChange({pole:val.pole,val:e.target.value})} value={this.state.form[val.pole]}/>
                </label>
            )
        })
        console.log(this.state,'state');
        
        return(
            <div className="CreatePotoks">
                {
                    create?(
                        <div className="form">
                            {mapForm}
                            <button onClick={this.create}>Создать</button>
                        </div>
                    ):(
                        <button onClick={()=>this.setState({create:true})}>Создать группу</button>
                    )
                }
            </div>
        )
    }
}