const rest = require('express-restify-mongoose');
var User = require('../schema/User');

module.exports = function(router) {

    return rest.serve(router, User, {
        name:'/account',
        contextFilter: (model, req, done) => {
            done(model.find({
                maler: true
            }))
          }
    })
  
  }