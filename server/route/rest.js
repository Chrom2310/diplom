const express = require('express');
const router = express.Router()
const File = require('../schema/File');
const fs = require('fs');
const multer = require('multer');
const mime = require('mime-types');
const uuidv1 = require('uuid/v1');


var storage = multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, '/home/vyacheslav/www/diplom/server/tmp')
    },
    filename: function (req, file, cb) {
      const ext = mime.extension(file.mimetype)
      cb(null, uuidv1() + '.' + ext)
    }
})

const uploader = multer({ storage: storage })

router.post('/upload',uploader.single('file'),async function(req,res){
    const fileInfo = req.file
    console.log(fileInfo,'fileInfo');
    console.log(req.header('user'),'req.header');
    const user = req.header('user');
    const type = req.header('type');

    try {      
        if (!fileInfo) return res.json({error:'Uploading file is failed. Empty field.'})            
        fileInfo.url = `http://192.168.0.4:3001/tmp/${fileInfo.filename}`;
        const result = await File.replaceOne({_id:fileInfo.filename,user:user,type:type,filename:fileInfo.originalname},fileInfo,{upsert:true})
        console.info('UPLOAD:',fileInfo.url)
        
        res.json({url:fileInfo.url})
      } catch (e) {
        if (fileInfo && fileInfo.filename) console.warn('TODO: remove file if exception',fileInfo.filename)
        console.error(e.message)
        res.json({error:'Uploading file is failed'})      
  
      }
})

module.exports = router;