const express = require('express');
const router = express.Router();
const Setting = require('../schema/Setting');

router.get('/getSettings',(req,res)=>{
    res.json(200)
})

router.get('/default' , (req,res)=>{
    var set = Setting();
    console.log(set);
    set.save();
    res.json(200)
})

router.get('/getRole' , async (req,res)=>{
    set = await Setting.findOne({"active":true});
    console.log(set);
    res.json(set.role);
})

module.exports = router;