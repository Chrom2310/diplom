const express = require('express');
const router = express.Router();
const Potoc = require('../schema/Potoc');
const PotocLib = require('../lib/Potoc');

router.post('/createPotoc', async(req,res)=>{
    var body = req.body;
    let result = await PotocLib.createPotoc(body)
    console.log(result,'result');
    res.json(result)
})

router.post('/addStydent' , async(req,res)=>{
    var body = req.body;
    let result = await PotocLib.addStudent(body);
    res.json(result);
})

module.exports = router;