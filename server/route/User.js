var express = require('express');
var router = express.Router();
var Users = require('../lib/Users');


router.post('/createUser', async(req,res)=>{
    console.log(req.body,'body');    
    let result = await Users.CreateUsers({body:req.body})
    res.json(result)
})

router.get('/AuthUser/:login', async (req,res)=>{
    console.log(req.params,'params');
    let login = req.params.login;
    let token = await Users.authUser({login}); 
    res.json({token:token});
})

router.post('/addPredmets', async (req,res)=>{
    var { login , crp , items } = req.body ;
    let result = await Users.addPredmets({login,predmets:{crp,items}});
    res.json(result);
})

router.post('/upUser', async (req,res)=>{
    var {id,form} = req.body;
    let result = await Users.updateUser({id,body:form});
    res.json(result)
})

router.get('/aut' , async (req,res)=>{
    console.log(req.query,'params');
    const { login , password } = req.query;
    let result = await Users.aut({login,password})
    console.log(result,'result');
    res.json(result)
})


module.exports = router;