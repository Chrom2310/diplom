const express = require('express');
const router = express.Router();
const User = require('./User');
const Settings =  require('./Settigs');
const Potoc = require('./Potoc');
const rest = require('./rest');
const restify = require('express-restify-mongoose')

function onRestError(err, req, res) {
    console.error(err);
    const statusCode = req.erm.statusCode;
    let result = { message: err.message || 'Unknown error' };
  
    if (err.name === 'ValidationError' && err.errors) {
      result = { errorMessages: mapValues(err.errors, e => e.message) };
    }
  
    res.status(statusCode).json(result);
  }

  restify.defaults({
    prefix:'',
    version:'',
    plural: true,
    private:['__v'],
    lowercase: true,
    totalCountHeader: false,
    findOneAndUpdate: false,
    findOneAndRemove: false,
    access: (req) => {
        let tree = req.erm.model.schema.tree
        if (tree.owner && tree.owner.ref === 'User') console.log('Request OWNERED Model')
        return 'public'
    },
    onError: onRestError
})

router.use("/v1",User,Settings,Potoc,rest)

module.exports = router;