var express = require('express');
var app = express();
var bodyParser = require('body-parser');
// var User = require('./route/User');
const Api = require('./route/api');
var bluebird  = require('bluebird');
var mongoose = require('mongoose');
var cors = require('cors');
app.use(cors());
const router = express.Router()
const restify = require('express-restify-mongoose')
const User = require('./schema/User');
const Potoc = require('./schema/Potoc');

restify.serve(router, User,{
    name:'account',
    contextFilter: (model, req, done) => {
        done(model.find({
            maler: true
        }))
      }
})

restify.serve(router,Potoc,{
    name:"potoks"
})

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use('/api/v1/',router)
app.use('/api',Api);
app.use('/tmp',express.static('tmp'));


app.listen(3001, function () {
    mongoose.connect('mongodb://localhost:27017/diplom',{
        useNewUrlParser: true,
        promiseLibrary: bluebird
    })
    console.log('Example app listening on port 3001!');
  });
