const SchemaPotoc = require('../schema/Potoc');
const User = require('../schema/User');

class Potoc {

    async createPotoc(body){
        var user = await User.findOne({"login":body.parent,"maler":true,"role":2})
        
        if (user){
            let potoc = SchemaPotoc(body);
            potoc.save();
            return 200;
        }else return 404
    }

    async addStudent(body){
        let user = await User.findOne({"login":body.user,"maler":true});
        let potok = await SchemaPotoc.findOne({name:body.potok,parent:body.parent});
        let up = {
            AllStudent:this.pushStudent({user,potoc:potok._doc.AllStudent})
        }
        console.log(up);
        
        let UPpotoc = await SchemaPotoc.update({name:body.potok,parent:body.parent},{$set:{...up}},{multi:true});
        if (UPpotoc){
            return 200;
        }else return "err"
    }

    pushStudent({user,potoc}){
        let newUser = {
            ...user._doc,
            sb:this.sb(user._doc.predmets),
        }
        potoc.push(newUser) 
        return potoc.sort(function(a,b){
            if (a.sb > b.sb){
                return -1;
            }
            if(a.sb < b.sb){
                return 1;
            }
            return 0 ;
        })
    }

    clone(array){
        return array.slice(0);
    };
    sb(mas){
        
        let bal = 0;
        Object.keys(mas).forEach(element => {
            let el = mas[element]; 
           bal = bal+Number(el.number) 
        });
        return bal / Object.keys(mas).length;
    }
    setPassStudents({AllStudent,max}){
        let newPassStudents = this.clone(AllStudent).sort(function(a,b){
            if (a.srp < b.srp){
                return -1;
            }
            if(a.srp > b.srp){
                return 1;
            }
            return 0 ;
        });

        let mas1 = newPassStudents.slice(0,max-1);
        let mas2 = newPassStudents.slice(max,newPassStudents.length);

        return mas1;
    }

}

var potoc = new Potoc();

module.exports = potoc;