var crypto = require('crypto');
var algorithm = 'aes-256-ctr',password = 'd6F3Efeq';
var User = require('../schema/User');
var mailerSend = require('../lib/Maler');
var jwt = require('jsonwebtoken');

class Users {
    encrypt(text){
        var cipher = crypto.createCipher(algorithm,password)
        var crypted = cipher.update(text,'utf8','hex')
        crypted += cipher.final('hex');
        return crypted;
      }
       
    decrypt(text){
        var decipher = crypto.createDecipher(algorithm,password)
        var dec = decipher.update(text,'hex','utf8')
        dec += decipher.final('utf8');
        return dec;
      }
      async CreateUsers({body}){
          if(body.password){
            body.password = this.encrypt(body.password);
            body.maler = false;
            let user = User(body);            
            try{
              let res = await user.save();
              if(res){
              //  setInterval(()=>{
                mailerSend.sendUser({to:body.email,
                login:body.login,
                password:this.decrypt(body.password)}) 
              //  },1000)
                
              }
              return 200;
            }
            catch(e){
              return "err"
            }
          }else{
              return "err"
          }
      }
      
      async authUser({login}){
        try{
          let user = await User.findOne({"login":String(login)})
          let res = await user.toAuthJSON();
          await User.update({login:res.login},{$set:{...res}},{multi:true})
          return res.token
        }
        catch(e){
          return "err"
        }
        
        
      }
      async updateUser({id,body}){
        try{
          await User.update({_id:id},{$set:{...body}},{multi:true});
          return 200;
        }catch(e){
          return "err"
        }
      }
      async addPredmets({login,predmets}){
        try{
          await User.update({login:login},{$set:{...predmets}},{multi:true});
          return 200;
        }
        catch(e){
          return "err"
        }
      }
    async aut({login,password}){
      try{        
        const user = await User.findOne({login:login});
        console.log(user.token,'user');
        
        if (user){
          const DecodP = this.decrypt(user.password)
          console.log(DecodP === password);
          
          return DecodP === password ? {status:200,user:user}:"err"
        }else return "err"
        
      }
      catch(e){
        return "err"
      }
    }
}

const users = new Users()

module.exports = users;