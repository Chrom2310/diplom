var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var Setting = new Schema({
slider:Array,
role:{
    type:Array,
    default:[
        {
            roleNumber:0,
            roleStringsRus:"Абитуриент"
        },
        {
            roleNumber:1,
            roleStringsRus:"Студент"
        },
        {
            roleNumber:2,
            roleStringsRus:"ВУЗ"
        },{
            roleNumber:3,
            roleStringsRus:"admin"
        }
    ]
},
type:{
    type:String,
    default:'default',
},
},{strict: false})

var setting = mongoose.model('Setting',Setting);

module.exports = setting;