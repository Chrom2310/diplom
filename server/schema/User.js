var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var uniqueValidator = require('mongoose-unique-validator');
var jwt = require('jsonwebtoken');

var User = new Schema({
    login:{type: String, lowercase: true, unique: true, required: [true, "can't be blank"], match: [/^[a-zA-Z0-9]+$/, 'is invalid'], index: true},
    password:String,
    email:{type: String, lowercase: true, unique: true, required: [true, "can't be blank"], match: [/\S+@\S+\.\S+/, 'is invalid'], index: true},
    maler:Boolean,
    role:{
        type:Number,
        default:0
    }
},{strict: false,timestamps:true});

User.plugin(uniqueValidator, {message: 'is already taken.'});

User.methods.generateJWT = function() {
    var today = new Date();
    var exp = new Date(today);
    exp.setDate(today.getDate() + 60);
    return jwt.sign({
    id: this._id,
    login: this.login,
    exp: parseInt(exp.getTime() / 1000*60*60*24*30),
    },"HS256");
};
// пила макита lr3050T - 8781 ;
// полотно дерево , метал
User.methods.toAuthJSON = function(){
    return {
    login: this.login,
    email: this.email,
    token: this.generateJWT(),
    maler:true
    };
};

var user = mongoose.model('User',User);

module.exports = user;