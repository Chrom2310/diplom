var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var potoc = new Schema({
    name:String,
    AllStudent:Array,
    NumOfFreePlaces:Number,
    NumOfPaidPlaces:Number,
    Open:Boolean,
    parent: String
},{strict: false,timestamps:true})

potoc.methods.create = function(body){
    var Potoc = potoc(body);
    Potoc.save();
}

Potoc = new mongoose.model('Potoc',potoc);

module.exports = Potoc;