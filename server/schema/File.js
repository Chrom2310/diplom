var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var file = new Schema({
    _id:String,
    name:String,
    type:String,
    user:Schema.ObjectId
},{strict:false})

var File = new mongoose.model('File',file);

module.exports = File;